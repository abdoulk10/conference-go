from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):
    # Use the Pexels API
    headers = {'Authorization': PEXELS_API_KEY}
    url = 'https://api.pexels.com/v1/search'
    payload = {
        'query': f'{city} {state}',
        'per_page': 1,
    }
    response = requests.get(url, params=payload, headers=headers)
    photo_dict = json.loads(response.content)
    return photo_dict['photos'][0]['url']


def get_weather_data(city, state):
    headers = {'Authorization': OPEN_WEATHER_API_KEY}
    geocoding_url = 'https://api.openweathermap.org/data/2.5/weather'
    geocoding_payload = {
        'query': f'{city} {state}',
    }
    geocoding_response = requests.get(
        geocoding_url,
        params=geocoding_payload,
        headers=headers,
        )
    geocoding_data = geocoding_response.json()
    latitude = geocoding_data['coord']['lat']
    longitude = geocoding_data['coord']['lon']

    weather_url = 'https://api.openweathermap.org/data/2.5/weather'
    weather_payload = {
        'query': f'{latitude} {longitude}',
    }
    weather_response = requests.get(
        weather_url,
        params=weather_payload,
        headers=headers,
        )
    weather_data = weather_response.json()
    temperature = weather_data['main']['temp']
    description = weather_data['weather'][0]['description']
    weather_info = {
        'temperature': temperature,
        'description': description
    }
    return weather_info
